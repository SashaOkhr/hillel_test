import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public class Main {

    public static void main(String[] args) {

        //1 task
        System.out.println(allDistinctWords(new File("C:\\Users\\User\\IdeaProjects\\hillel_test\\src\\file.txt")));

        //2 task
        ArrayList<Integer> list = new ArrayList<>();
        arrayToTheCollection(new Integer[]{2, 3, 4}, list);
        System.out.println(list);
    }

    static Set<String> allDistinctWords(File file) {

        Set<String> result = new HashSet<>();

        try (Scanner scanner = new Scanner(file)) {
            while (scanner.hasNext()) {
                String line = scanner.nextLine().toLowerCase();
                Scanner lineScanner = new Scanner(line);
                lineScanner.useDelimiter(" ");

                while (lineScanner.hasNext()) {
                    String word = lineScanner.next();
                    result.add(word);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    static <T> void arrayToTheCollection(T[] array, ArrayList<T> list) {
        for (T element : array) {
            list.add(element);
        }
    }

    //3 task
    public class IncrementSynchronize {
        private int value = 0;

        public synchronized int getNextValue1() {
            return value++;
        }

        public int getNextValue2() {
            synchronized (this) {
                return value++;
            }
        }

        public int getNextValue3() {
            AtomicInteger value = new AtomicInteger(this.value);
            return value.incrementAndGet();
        }
    }
}
